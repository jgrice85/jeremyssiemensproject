# About this app

## This app is built using

### Frontend

* Vue
* Vue Bootstrap
* Vue Carousel

### Backend

* Node
* Express


## To run this project

Navigate to the project root directory 

```
npm install
```

This will install the required dependencies.

Next, run

```
Node server.js
```

This will start the express server. 

Lastly run

```
npm run serve
```

### Original project documentation below

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


# README #

This project requires you to develop a website template viewer. The template viewer is modeled after the filmstrip folder view in Windows Explorer.

### Technical Requirements ###

* Develop as a Single Page Application
* Develop both a front-end component and back-end API to retrieve the data.
* Use a javascript front-end implementation of your choice, some suggestions might be ReactJS, VueJS, or AngularJS
* Create a back-end API implementation with NodeJS/Express

### Reference ###
![Example Filmstrip](https://bitbucket.org/repo/kMx7k8n/images/3768382958-2017-09-26_9-09-27.png)

### Feature Requirements ###

* Display the thumbnail images in a filmstrip view below the main large image
* Set the thumbnail to have a sliding window with 4 thumbnails visible in the window at a time. 
* Implement a "next" and "previous" link per the styles provided. The sliding window is not circular, when the first 4 thumbnails appear, the previous link should be disabled. When the end of the thumbnail set is reached, the next link should be disabled.
  Note: The sliding window may not have a total of 4 thumbnails if the total template count is not evenly divisible by 4. There are 15 templates in the reference data.