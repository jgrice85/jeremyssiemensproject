const express = require('express');

const cors = require('cors');

const app = express();

const fs = require('fs');

const ourData = fs.readFileSync('./data/templates.json');

const parsedData = JSON.parse(ourData);

app.use(cors());

app.get('/', (req, res) => {
  res.json(parsedData);
});

app.listen(3000, () => {
  console.log('This worked!');
});
