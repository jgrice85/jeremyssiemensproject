import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import VueCarousel from 'vue-carousel';
import App from './App.vue';

Vue.use(BootstrapVue, VueCarousel);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
